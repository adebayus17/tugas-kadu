import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Footer from "../component/footer/Footer";
import Navbar from "../component/navbar/Navbar";
import AboutMe from "../pages/AboutMe";
import HomePage from "../pages/HomePage";
import PokemonPage from "../pages/PokemonPage";

function App() {
	return (
		<BrowserRouter>
			<Navbar />
			<Routes>
				<Route path="/" element={<Navigate to="/homepage" />} />
				<Route path="/homepage" element={<HomePage />} />
				<Route path="/pokemon" element={<PokemonPage />} />
				<Route path="/about" element={<AboutMe />} />
			</Routes>
			<Footer bgColor="#345b63" />
		</BrowserRouter>
	);
}

export default App;
