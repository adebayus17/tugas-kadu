import React from "react";
import { createUseStyles } from "react-jss";
import Container from "../component/Container";
import HomeContent from "../component/homecontent/HomeContent";
import { homeContents } from "../data/homeContents";
import { color } from "../styles/color";
import PageContainer from "./PageContainer";

const useStyles = createUseStyles({
	container: {
		width: "100%",
		maxWidth: "1400px",
		margin: "0 auto",
	},
	wrapper: {
		padding: "50px 50px",
		display: "flex",
		gap: "50px",
		flexDirection: "column",
	},
});

function HomePage() {
	const classes = useStyles();
	return (
		<PageContainer>
			<Container>
				<div className={classes.container}>
					<div className={classes.wrapper}>
						{homeContents.map((item, index) => (
							<HomeContent key={index} index={index} item={item} />
						))}
					</div>
				</div>
			</Container>
		</PageContainer>
	);
}

export default HomePage;
