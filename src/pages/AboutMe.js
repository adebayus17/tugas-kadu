import React from "react";
import Container from "../component/Container";
import PageContainer from "./PageContainer";
import FotoProfil from "../assets/fotoprofil.jpg";

function AboutMe() {
	return (
		<PageContainer>
			<Container height>
				<div
					style={{
						height: "100vh",
						width: "100%",
						maxWidth: "1400px",
						margin: "0 auto",
					}}
				>
					<div style={{ padding: "50px 50px", display: "flex", gap: "20px" }}>
						<div style={{ flex: "1 1 0" }}>
							<img
								src={FotoProfil}
								style={{
									width: "100%",
									height: "100%",
									objectFit: "cover",
								}}
							/>
						</div>
						<div
							style={{
								flex: "1 1 0",
								display: "grid",
								gridTemplateColumns: "1fr 1fr",
								gridTemplateRows: "1fr 1fr 1fr",
								gap: "10px",
							}}
						>
							<span>Deskripsi Diri</span>
							<span>
								“Lulusan S1 dengan spesialisasi di pemasaran digital. Memiliki
								pengalaman dalam mengembangkan dan mengimplementasikan kampanye
								pemasaran digital melalui kesempatan magang di dua perusahaan
								multinasional. Mencari peluang kerja yang memberi kesempatan
								untuk semakin berkembang.”
							</span>
							<span>Background Pendidikan</span>
							<ol style={{ margin: 0 }}>
								<li>S1 Universtias ... </li>
								<li>D3 Universtias ...</li>
								<li>SMA ... </li>
							</ol>
							<span>Pengalaman</span>
							<span>Belum Ada </span>
						</div>
					</div>
				</div>
			</Container>
		</PageContainer>
	);
}

export default AboutMe;
