import React from "react";
import CardPokemon from "../component/CardPokemon";
import Container from "../component/Container";
import { pokeList } from "../data/pokeList";
import PageContainer from "./PageContainer";

function PokemonPage() {
	return (
		<PageContainer>
			<Container>
				<div
					style={{
						height: "100vh",
						width: "100%",
						maxWidth: "1400px",
						margin: "0 auto",
					}}
				>
					<div
						style={{
							padding: "50px 50px",

							height: "100%",
						}}
					>
						<h1 style={{ textAlign: "center" }}>Pokemon List</h1>
						<div
							style={{
								marginTop: "50px",
								display: "grid",
								gridTemplateColumns: "1fr 1fr 1fr",
								gap: "50px",
							}}
						>
							{pokeList.map((item, index) => (
								<CardPokemon key={index} item={item} />
							))}
						</div>
					</div>
				</div>
			</Container>
		</PageContainer>
	);
}

export default PokemonPage;
