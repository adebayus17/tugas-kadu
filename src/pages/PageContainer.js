import React from "react";

function PageContainer(props) {
	return <div style={{ minHeight: "100vh" }}>{props.children}</div>;
}

export default PageContainer;
