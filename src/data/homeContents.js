import logobannerTwo from "../assets/banner2.png";
import logobannerFive from "../assets/banner5.png";
import logobannerSven from "../assets/banner7.png";

export const homeContents = [
	{
		title: "Bermain Dan Berpetualang Untuk Menjadi Trainer Terkuat",
		logo: logobannerTwo,
	},
	{
		title: "Tangkap Monster Dan Berpetualang Bersamanya",
		logo: logobannerFive,
	},
	{ title: "Kumpulkan Dan Koleksi Monster Monster Kuat", logo: logobannerSven },
];
