import charizad from "../assets/pokemon/charizad.jpeg";
import evee from "../assets/pokemon/evee.jpeg";
import garchomp from "../assets/pokemon/garchomp.jpeg";
import gardevoir from "../assets/pokemon/gardevoir.jpeg";
import lucario from "../assets/pokemon/lucario.jpeg";
import pikachu from "../assets/pokemon/pikachu.jpeg";
import snorlax from "../assets/pokemon/snorlax.jpeg";

export const pokeList = [
	{
		title: "Charizad",
		img: charizad,
	},
	{
		title: "Eevee",
		img: evee,
	},
	{
		title: "Garchomp",
		img: garchomp,
	},
	{
		title: "GardeVoir",
		img: gardevoir,
	},
	{
		title: "Lucario",
		img: lucario,
	},
	{
		title: "Pikachu",
		img: pikachu,
	},
	{
		title: "Snorlax",
		img: snorlax,
	},
];
