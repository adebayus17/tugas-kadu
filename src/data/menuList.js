export const menuList = [
	{ title: "Homepage", to: "/homepage" },
	{ title: "Pokemon", to: "/pokemon" },
	{ title: "About Me", to: "/about" },
];
