import React from "react";
import { createUseStyles } from "react-jss";
import Container from "../Container";
import logoKawah from "../../assets/logo-kawah-edukasi-normal.svg";
import logoTwitter from "../../assets/twitter.png";
import logoInstagram from "../../assets/instagram.png";
import logoGithub from "../../assets/github.png";
import logoFacebook from "../../assets/facebook.png";

const useStyles = createUseStyles({
	container: {
		width: "100%",
		maxWidth: "1400px",
		margin: "0 auto",
	},
	wrapper: {
		padding: "50px 50px",
		height: "100%",
		display: "flex",
	},
	footerLeft: {
		gap: "20px",
		display: "flex",
		flexDirection: "column",
		flex: "5 1",
	},
	socialContainer: {
		display: "flex",

		gap: "10px",
		"& img": {
			width: "30px",
			height: "30px",
		},
	},
	footerRight: {
		flex: "1 1",
		display: "flex",
		gap: "20px",
		flexDirection: "column",
		alignItems: "start",
		justifyContent: "start",
		height: "100%",
		"& img": {
			width: "30px",
		},
		"& a": {
			textDecoration: "none",
			color: "white",
		},
		"& span": { marginLeft: "5px", fontSize: "34px" },
	},
});

function Footer({ bgColor, ...props }) {
	const classes = useStyles();

	return (
		<Container bgColor={bgColor}>
			<div className={classes.container}>
				<div className={classes.wrapper}>
					<div className={classes.footerLeft}>
						<img src={logoKawah} width="250" />
						<span>
							M-SQUARE Jl.Cibaduyut No.142 Ruko Blok C<br />
							No.23 Bandung - Jawa Barat Indonesia
						</span>
						<div class={classes.socialContainer}>
							<a href="#">
								<img src={logoInstagram} alt="instagram" />
							</a>
							<a href="#">
								<img src={logoFacebook} alt="facebook" />
							</a>
							<a href="#">
								<img src={logoTwitter} alt="twitter" />
							</a>
						</div>
					</div>
					<div className={classes.footerRight}>
						<span>Contact Me</span>
						<a href="https://github.com/adebayus" target="_blank">
							<img src={logoGithub} alt="Github" />
							<span>Github</span>
						</a>
					</div>
				</div>
			</div>
		</Container>
	);
}

export default Footer;
