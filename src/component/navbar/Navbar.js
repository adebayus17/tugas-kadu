import React from "react";
import { createUseStyles } from "react-jss";
import NekoLogo from "../../assets/meowth.png";
import Container from "../Container";
import MenuItem from "./MenuItem";
import { menuList } from "../../data/menuList";
import { useResolvedPath } from "react-router-dom";

const localStyles = createUseStyles({
	container: {
		width: "100%",
		maxWidth: "1400px",
		margin: "0 auto",
		height: "70px",
	},
	wrapper: {
		padding: "0 50px",
		height: "100%",
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
	},
	leftBar: {
		flex: "1 1 50%",
		display: "flex",
		alignItems: "center",
		gap: "10px",
		"& img": {
			width: "50px",
			height: "50px",
		},
		"& span": {
			fontSize: "22px",
		},
	},
	rightBar: {
		flex: "1 1 50%",
		display: "flex",
		justifyContent: "flex-end",
		alignItems: "center",
		gap: "10px",
	},
});

function Navbar() {
	const classes = localStyles();
	return (
		<Container>
			<div className={classes.container}>
				<div className={classes.wrapper}>
					<div className={classes.leftBar}>
						<img src={NekoLogo} />
						<span>PokePokeDex</span>
					</div>
					<div className={classes.rightBar}>
						{menuList.map((item, index) => (
							<MenuItem key={index} to={item.to}>
								{item.title}
							</MenuItem>
						))}
					</div>
				</div>
			</div>
		</Container>
	);
}

export default Navbar;
