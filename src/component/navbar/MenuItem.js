import React from "react";
import { createUseStyles } from "react-jss";
import { Link, useMatch, useResolvedPath } from "react-router-dom";
import { color } from "../../styles/color";

const useStyle = createUseStyles({
	item: {
		padding: "10px 25px",
		border: "1px solid white",
		borderRadius: "8px",
		cursor: "pointer",
		"&:hover": {
			backgroundColor: "white",
			color: color.articsNight,
		},
	},
	active: {
		border: "1px solid white",
		backgroundColor: "white",
		color: color.articsNight,
	},
});

function MenuItem({ to, ...props }) {
	const path = useResolvedPath(to);
	const match = useMatch({ path: path.pathname, end: true });
	const classes = useStyle();

	return (
		<Link to={to} style={{ textDecoration: "none", color: "white" }}>
			<span className={`${classes.item} ${match ? classes.active : ""}`}>
				{props.children}
			</span>
		</Link>
	);
}

export default MenuItem;
