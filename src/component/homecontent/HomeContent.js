import React from "react";
import { createUseStyles } from "react-jss";

const useStyle = createUseStyles({
	wrapper: {
		display: "flex",
		gap: "20px",
		flexDirection: (props) =>
			props.direction ? props.direction : "row-reverse",
	},
	imgContainer: {
		flex: "1.1 1 0",
		"& img": {
			border: "1px dashed pink",
			borderRadius: "6px",
			width: "100%",
			height: "600px",
			objectFit: "cover",
		},
	},
	textContainer: {
		flex: "1 1 0",
		display: "flex",
		alignItems: "center",
		"& span": {
			fontSize: "40px",
			fontWeight: "700",
			textAlign: "center",
		},
	},
});

function HomeContent({ item, index, ...props }) {
	const classes = useStyle(index % 2 == 0 && { direction: "row" });

	return (
		<div className={classes.wrapper}>
			<div className={classes.imgContainer}>
				<img src={item.logo} />
			</div>
			<div className={classes.textContainer}>
				<span>{item.title}</span>
			</div>
		</div>
	);
}

export default HomeContent;
