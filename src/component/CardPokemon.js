import React from "react";

function CardPokemon(props) {
	return (
		<div style={{ flex: "1 1 ", justifyContent: "center" }}>
			<div
				style={{
					borderRadiuse: "5px",
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
			>
				<img src={props.item.img} width="92px" height="92px" />
				<span>{props.item.title}</span>
			</div>
		</div>
	);
}

export default CardPokemon;
