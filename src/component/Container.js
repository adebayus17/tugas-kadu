import React from "react";
import { createUseStyles } from "react-jss";
import { color } from "../styles/color";

const useStyles = createUseStyles({
	container: {
		width: "100%",
		backgroundColor: ({ bgColor, ...props }) => {
			console.log(bgColor);
			return bgColor ? bgColor : color.blackCounts;
		},
		height: ({ height }) => (height ? "100%" : ""),
	},
});

function Container({ bgColor, height, ...props }) {
	console.log(bgColor);
	const classes = useStyles({ bgColor, height });
	return <div className={classes.container}>{props.children}</div>;
}

export default Container;
