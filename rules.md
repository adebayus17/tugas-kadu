Task:
Migrasikan Web Pokedex yang sudah di pada styling-chellenge ke dalam react Project.

- Tambahkan navigasi pada Navigari Bar.
- Path '/' untuk menu Home.
- Path '/pokemon' untuk menu Pokemon. Menu ini digunakan untuk menampilkan list pokemon.
- Path '/about' untuk menu About Me. Menu ini harus memiliki 4 bagian yaitu:
  a. Foto diri (minimal 1).
  b. Deskripsi diri
  c. Background pendidikan
  d. Pengalaman

Rules:
1. Dalam setiap page harus terdapat Navigasi Bar dan Footer.
2. Foto diri menggunakan internal source.
3. Menerapkan konsep display.
4. Dikumpulkan paling lambat hari Minggu, 24 Juni 2022 pukul 18.00 WIB.

Note: 
Setelah membuah nama branch silahkan tulihkan perintah "npx create-react-app ." atau "npx create-react-app@latest ."
